package v2

import (
	"time"

	"testing"

	"github.com/stretchr/testify/assert"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/config"
)

func TestRemoteJSON(t *testing.T) {
	param := defaultXParam
	param.Namespace = x2Json
	initErr := InitWithParam(param)
	assert.NoError(t, initErr)

	namespaceList := splitNamespace(param.Namespace)
	assertWithNamespace(t, namespaceList...)

	waitWriteConfigFile()
}

func TestRemoteYaml(t *testing.T) {
	param := defaultXParam
	param.Namespace = xconfServerYaml

	initErr := InitWithParam(param)
	assert.NoError(t, initErr)

	namespaceList := splitNamespace(param.Namespace)
	assertWithNamespace(t, namespaceList...)

	waitWriteConfigFile()
}

func TestRemoteProperties(t *testing.T) {
	param := defaultXParam
	param.Namespace = applicationProperties

	initErr := InitWithParam(param)
	assert.NoError(t, initErr)

	namespaceList := splitNamespace(param.Namespace)
	assertWithNamespace(t, namespaceList...)

	waitWriteConfigFile()
}

func TestRemoteWithXconfYaml(t *testing.T) {
	filePath := "./xconf.yaml"
	err := InitWithConfigFile(filePath)
	assert.NoError(t, err)

	xParam := XParam{}
	err = LoadConfigWithPassphrase(filePath, &xParam, "")
	assert.NoError(t, err)

	namespaceList := splitNamespace(xParam.Namespace)
	assertWithNamespace(t, namespaceList...)
}

func TestConcurrencyXconfServerYaml(t *testing.T) {
	param := defaultXParam
	param.Namespace = xconfServerYaml

	initErr := InitWithParam(param)
	assert.NoError(t, initErr)

	app := Application{}
	xc, err := GetConfig(xconfServerYaml, &app)
	assert.NoError(t, err)
	t.Logf("Application properties: %+v", app)

	for i := 0; i < 100000; i++ {
		go func() {
			config.DebugConcurrency(xconfServerYaml)
		}()
		go func() {
			assertDynamicXconfServerYaml(t, xc)
			//s1 := xc.GetString("s1")
			//t.Logf("s1:=%s", s1)
		}()
		time.Sleep(time.Millisecond)
	}

	waitWriteConfigFile()
}

// 等待配置文件写入本地
func waitWriteConfigFile() {
	time.Sleep(time.Second * 2)
}
