package v2

import (
	"fmt"
	"strings"
	"time"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/config"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/logger"

	"testing"
)

type NamespaceType = string
type testAssertFun func(t *testing.T)

var testMap = map[NamespaceType]testAssertFun{
	applicationProperties: assertApplication,
	xconfServerYaml:       assertXconfServerYaml,
	x2Json:                assertX2Json,
}

const (
	applicationProperties NamespaceType = "application.properties"
	x2Json                NamespaceType = "x2.json"
	xconfServerYaml       NamespaceType = "xconf_server.yaml"
)

const (
	remoteIP  = "192.168.99.252:8080"
	appID     = "xconf_server"
	backupDir = "./tmp"
)

var defaultXParam = XParam{
	Ip:        remoteIP,
	AppId:     appID,
	BackupDir: backupDir,
}

const namespaceSplitStr = ","

func getSplitCommaNamespace(namespaceType ...NamespaceType) string {
	return strings.Join(namespaceType, namespaceSplitStr)
}

func splitNamespace(namespace string) []NamespaceType {
	return strings.Split(namespace, namespaceSplitStr)
}

func init() {
	logger.SetLevel(logrus.TraceLevel)
}

type RedisConf struct {
	Address string `mapstructure:"address"`
	Auth    string `mapstructure:"auth"`
	Db      int    `mapstructure:"db"`
}

type MongoConf struct {
	Dsn    string `mapstructure:"dsn"`
	DbName string
}

type Conf struct {
	Redis   map[string]RedisConf `mapstructure:"redis"`
	Mongo   map[string]MongoConf `mapstructure:"mongo"`
	AppName string               `mapstructure:"app_name"`
}

type Application struct {
	S1 string `mapstructure:"s1"`
	M1 string `mapstructure:"m1"`
	U1 string `mapstructure:"u1"`
}

func assertWithNamespace(t *testing.T, namespaceType ...NamespaceType) {
	for _, item := range namespaceType {
		if v, ok := testMap[item]; ok {
			v(t)
		}
	}
}

func checkConfigObject(configObj Conf, t *testing.T) {
	assert.NotEmpty(t, configObj.Redis["content_filter"])
	assert.NotEmpty(t, configObj.Mongo["content_filter"])

	assert.Equal(t, "mongodb://120.25.66.46:27077", configObj.Mongo["content_filter"].Dsn)
	assert.Equal(t, "redis-0.xiaoniangao.cn:6379", configObj.Redis["content_filter"].Address)
	assert.Equal(t, "112", configObj.Redis["content_filter"].Auth)
	assert.Equal(t, 2, configObj.Redis["content_filter"].Db)
}

func init() {
	SetLogLevel(LevelInfo)
}

func TestLoadConfig(t *testing.T) {
	// 追踪配置文件的每一步加载
	SetLogLevel(LevelTrace)

	filePath := "./conf.yaml"
	configObj := Conf{}
	err := LoadConfig(filePath, &configObj)
	if err != nil {
		t.Fatal(err)
	}
	checkConfigObject(configObj, t)
}

func TestXConfig(t *testing.T) {
	xConfig := New()
	xConfig.SetConfigName("conf")
	xConfig.SetConfigType("yaml")
	xConfig.AddConfigPath(".")
	err := xConfig.ReadInConfig() // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		t.Fatal(err)
	}
	configObj := Conf{}
	err = xConfig.Unmarshal(&configObj)
	if err != nil {
		t.Fatal(err)
	}

	checkConfigObject(configObj, t)

	appName := xConfig.GetString("app_name")
	redisDb := xConfig.GetInt("redis.content_filter.db")
	mongoMap := xConfig.GetStringMap("mongo")

	assert.Equal(t, true, len(mongoMap) > 0)
	assert.Equal(t, "XConfig SDK", appName)
	assert.Equal(t, 2, redisDb)
}

type CDNSwitch struct {
	Name        string `mapstructure:"name"`
	ReplaceWith string `mapstructure:"replace_with"`
}

func assertX2Json(t *testing.T) {
	app := Application{}
	xc, err := GetConfig(x2Json, &app)
	assert.NoError(t, err)
	t.Logf("x2.json: %+v", app)

	s1 := xc.GetString("s1")
	m1 := xc.GetString("m1")
	u1 := xc.GetString("u1")

	assert.NotEmpty(t, s1)
	assert.NotEmpty(t, m1)
	assert.NotEmpty(t, u1)
}

func assertApplication(t *testing.T) {
	app := Application{}
	xc, err := GetConfig(applicationProperties, &app)
	assert.NoError(t, err)
	t.Logf("Application properties: %+v", app)

	s1 := xc.GetString("s1")
	m1 := xc.GetString("m1")
	u1 := xc.GetString("u1")

	// 如果支持apollo配置时可以输入换行符，则需要修改 xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo 下的:
	// @link func convertToProperties(cache agcache.CacheInterface) string
	// 担心会有其他影响，先不修改，可以改为XClientConfigWith接口获取属性配置
	c99 := xc.GetString("c_99000") // 有换行符会影响到viper property的加载
	t.Logf("s1:%s, c99:%s", s1, c99)

	x1 := config.XClientConfigWith(applicationProperties)
	//o := x1.GetProperty("c_99000")
	//t.Logf("%v", o)

	for a := 0; a < 100; a++ {
		o := x1.GetProperty("c_99000")
		fmt.Println("-0-0-0->" + o.(string))
		time.Sleep(time.Second)
	}

	assert.NotEmpty(t, s1)
	assert.NotEmpty(t, m1)
	assert.NotEmpty(t, u1)
}

func assertXconfServerYaml(t *testing.T) {
	xc, err := GetConfig(xconfServerYaml, nil)
	assert.NoError(t, err)
	configObj := Conf{}
	err = xc.Unmarshal(&configObj)
	assert.NoError(t, err)

	assert.NotEmpty(t, configObj.Redis["content_filter"])
	assert.NotEmpty(t, configObj.Mongo["content_filter"])

	assert.Equal(t, "mongodb://120.25.66.46:27077", configObj.Mongo["content_filter"].Dsn)
	assert.Equal(t, "redis-0.xiaoniangao.cn:6379", configObj.Redis["content_filter"].Address)
	assert.Equal(t, "112", configObj.Redis["content_filter"].Auth)
	assert.Equal(t, 2, configObj.Redis["content_filter"].Db)

	appName := xc.GetString("app_name")
	redisDb := xc.GetInt("redis.content_filter.db")
	mongoMap := xc.GetStringMap("mongo")

	assert.Equal(t, true, len(mongoMap) > 0)
	assert.Equal(t, "XConfig SDK", appName)
	assert.Equal(t, 2, redisDb)

	// 单独解析yaml中的复合字段
	cdnMap := make(map[string]CDNSwitch, 0)
	cdnSwitchErr := xc.UnmarshalKey("cdn_switch", &cdnMap)
	assert.NoError(t, cdnSwitchErr)
	t.Logf("[UnmarshalKey] => cdn_switch%+v", cdnMap)

	groupLimit := xc.GetInt64("group_limit")
	assert.Greater(t, groupLimit, int64(0))
	t.Logf("group_limit: %v", groupLimit)
}

func assertDynamicXconfServerYaml(t *testing.T, xc *XConfig) {
	appName := xc.GetString("app_name")
	redisDb := xc.GetInt("redis.content_filter.db")
	mongoMap := xc.GetStringMap("mongo")

	assert.Equal(t, true, len(mongoMap) > 0)
	assert.Equal(t, "XConfig SDK", appName)
	assert.Equal(t, 2, redisDb)

	// 单独解析yaml中的复合字段
	cdnMap := make(map[string]CDNSwitch, 0)
	cdnSwitchErr := xc.UnmarshalKey("cdn_switch", &cdnMap)
	assert.NoError(t, cdnSwitchErr)
	t.Logf("[UnmarshalKey] => cdn_switch%+v", cdnMap)

	groupLimit := xc.GetInt64("group_limit")
	assert.Greater(t, groupLimit, int64(0))
	t.Logf("group_limit: %v", groupLimit)
}
