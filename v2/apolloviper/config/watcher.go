package config

import (
	"sync"

	viper "xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
)

var watcherMap sync.Map

type Watcher struct {
	updated chan bool
}

func (c *Watcher) Watch(namespace string, stop chan bool) <-chan *viper.RemoteResponse {
	resp := make(chan *viper.RemoteResponse, 0)

	go func() {
		for {
			select {
			case <-stop:
				return
			case <-c.updated:
				value, err := GetContent(namespace)
				resp <- &viper.RemoteResponse{Value: value, Error: err}
			}
		}
	}()

	return resp
}

func NewWatcher(namespace string) *Watcher {
	watcher := &Watcher{updated: make(chan bool)}
	watcherMap.Store(namespace, watcher)
	return watcher
}
