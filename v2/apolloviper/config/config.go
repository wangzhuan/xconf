package config

import (
	"container/list"
	"errors"
	"fmt"
	"path"
	"strings"

	"xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo/constant"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/common"
)

var (
	changeListeners *list.List
)

func init() {
	changeListeners = list.New()
}

// AddChangeListener ...
func AddChangeListener(listener common.ChangeListener) {
	if listener == nil {
		return
	}
	changeListeners.PushBack(listener)
}

//push config change event
func pushChangeEvent(event *common.ChangeEvent) {
	// if channel is null ,mean no listener,don't need to push msg
	if changeListeners == nil || changeListeners.Len() == 0 {
		return
	}

	for i := changeListeners.Front(); i != nil; i = i.Next() {
		cListener := i.Value.(common.ChangeListener)
		go cListener.OnChange(event)
	}
}

// GetSuffix apollo配置文件默认的扩展名是properties
func GetSuffix(namespace string) string {
	suffix := strings.ToLower(path.Ext(namespace))
	if suffix == "" {
		suffix = fmt.Sprintf("%v", constant.Properties)
	} else {
		suffix = suffix[1:] //去掉前面的" . "
	}
	return suffix
}

// GetContent  获取namespace下的所有配置
func GetContent(namespace string) ([]byte, error) {
	compareName := namespace
	suffix := strings.ToLower(path.Ext(namespace))
	if suffix == "" {
		compareName = fmt.Sprintf("%s.%v", namespace, constant.Properties)
		suffix = fmt.Sprintf("%v", constant.Properties)
	}
	if strings.HasSuffix(compareName, suffix) {
		format := constant.ConfigFileFormat(suffix)
		s := apolloClient.GetConfig(namespace).GetContentWith(format)
		return []byte(s), nil
	}
	errMessage := fmt.Sprintf("apollo namespace:%v cache is nil", namespace)

	return nil, errors.New(errMessage)
}
