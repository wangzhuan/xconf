package config

import (
	"strings"
	"time"

	agollo "xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo"
	agolloConfig "xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo/env/config"
	"xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo/storage"
	viper "xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/common"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/logger"
)

var apolloClient agollo.Client

func StartWithConfig(appConfig *agolloConfig.AppConfig) {
	client, err := agollo.StartWithConfig(func() (*agolloConfig.AppConfig, error) {
		return appConfig, nil
	})
	if err != nil {
		panic(err)
	}
	apolloClient = client

	client.AddChangeListener(&listener)
	logger.Info("初始化Apollo配置成功")

}

type changeListener struct {
}

func (c *changeListener) OnNewestChange(event *storage.FullChangeEvent) {
	logger.Info("OnNewestChange")
}

func (c *changeListener) OnChange(changeEvent *storage.ChangeEvent) {
	//bytes, _ := json.Marshal(changeEvent)
	//fmt.Println(string(bytes))
	namespace := changeEvent.Namespace
	if !strings.Contains(namespace, ".") {
		namespace = namespace + ".properties"
	}

	if value, ok := watcherMap.Load(namespace); ok {
		watcher := value.(*Watcher)
		watcher.updated <- true
	}

	// 延迟调用，等待viper同步
	time.AfterFunc(time.Millisecond*500, func() {
		pushChangeEvent(&common.ChangeEvent{Namespace: namespace})
	})
}

var listener = changeListener{}

func XClientConfigWith(namespace string) *XClientConfig {
	return &XClientConfig{
		viper:     nil,
		namespace: namespace,
	}
}

type XClientConfig struct {
	viper     *viper.Viper
	namespace string
}

func (x XClientConfig) GetProperty(key string) interface{} {
	if apolloClient == nil {
		return nil
	}
	return apolloClient.GetConfig(x.namespace).Get(key)
}

func (x XClientConfig) Get(key string, namespace string) interface{} {
	if apolloClient == nil {
		return nil
	}
	return apolloClient.GetConfig(namespace).Get(key)
}

func DebugConcurrency(namespace string) {
	if value, ok := watcherMap.Load(namespace); ok {
		watcher := value.(*Watcher)
		watcher.updated <- true
	}

}
