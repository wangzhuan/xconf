package config

import (
	viper "xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
)

type XConfigManager interface {
	Get(key string) ([]byte, error)
	Watch(key string, stop chan bool) <-chan *viper.RemoteResponse
}

// 需要实现crypt.ConfigManager接口，用以支持viper
type configManager struct {
	watcher *Watcher
}

// Get 获取namespace下的所有配置
func (c configManager) Get(namespace string) ([]byte, error) {
	return GetContent(namespace)
}

func (c configManager) Watch(namespace string, stop chan bool) <-chan *viper.RemoteResponse {
	return c.watcher.Watch(namespace, stop)
}

// NewApolloConfigManager 调用该方法前需要先完成apollo的初始化
func NewApolloConfigManager(namespace string) XConfigManager {
	watcher := NewWatcher(namespace)
	return &configManager{watcher: watcher}
}
