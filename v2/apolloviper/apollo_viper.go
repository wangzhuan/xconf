package apolloviper

import (
	"errors"
	"strings"

	agollo "xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo"
	agolloConfig "xgit.xiaoniangao.cn/xngo/lib/github.com-apolloconfig-agollo/env/config"
	"xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/config"
	_ "xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/remote"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/logger"
)

type ViperConfigMap map[string]*viper.Viper

var configMap ViperConfigMap

// InitViperConfigMap ...
// ip  http://192.168.99.252:8080
// cluster 默认为： default
func InitViperConfigMap(appID, ip, cluster, namespaceP, backupConfigPath string) {
	logPrefix := "[InitViperConfigMap]=> "
	agollo.SetLogger(&logger.XconfLogger{SDKName: "apollo"})
	logger.Info(logPrefix, "start init...")

	namespaceParam := formatNamespace(namespaceP)
	appConfig := &agolloConfig.AppConfig{
		AppID:            appID,
		Cluster:          cluster,
		IP:               ip,
		NamespaceName:    namespaceParam,
		IsBackupConfig:   true,
		BackupConfigPath: backupConfigPath,
	}
	config.StartWithConfig(appConfig)

	initViper(namespaceParam, ip)

	logger.Info(logPrefix, "end init")
}

func initViper(namespaceSplit, ip string) {
	logPrefix := "[initViper]=> "
	logger.Info(logPrefix, "start init...")

	namespaces := strings.Split(namespaceSplit, ",")
	configMap = make(ViperConfigMap)
	var vipers []*viper.Viper
	for _, namespace := range namespaces {
		suffix := config.GetSuffix(namespace)
		xViper := viper.New()
		xViper.SetConfigType(suffix)
		err := xViper.AddRemoteProvider("apollo", ip, namespace)
		if err != nil {
			logger.Errorf(logPrefix, "namespace: %v, add remote provider error", namespace, err.Error())
			panic(err)
		}

		// 读取provider中的配置
		err = xViper.ReadRemoteConfig()
		if err != nil {
			logger.Errorf(logPrefix, "namespace:%v, read remote config error", namespace, err.Error())
		}
		vipers = append(vipers, xViper)
		err = xViper.WatchRemoteConfigOnChannel()
		if err != nil {
			logger.Errorf(logPrefix, "namespace:%v, WatchRemoteConfigOnChannel error", namespace, err.Error())
			panic(err)
		}
		configMap[namespace] = xViper
	}

	logger.Info(logPrefix, "end init")
}

func GetConfigMap() ViperConfigMap {
	if len(configMap) < 1 {
		errMsg := "uninitialization. need call InitViperConfigMap"
		logger.Error(errMsg)
		err := errors.New(errMsg)
		panic(err)
	}
	return configMap
}

func formatNamespace(namespaceP string) string {
	items := strings.Split(namespaceP, ",")
	var arr []string
	for _, item := range items {
		item2 := strings.Trim(item, " ")
		arr = append(arr, item2)
	}
	return strings.Join(arr, ",")
}
