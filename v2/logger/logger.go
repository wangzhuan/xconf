package logger

import (
	"runtime"
	"strconv"
	"strings"

	nested "github.com/antonfisher/nested-logrus-formatter"
	"github.com/sirupsen/logrus"
	viper "xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
)

const (
	sdkField      = "SDK"
	filePathField = "Path"
)

var localLogger *logrus.Logger
var defaultLogger *XconfLogger

func init() {
	localLogger = loggerWith(logrus.InfoLevel)
	defaultLogger = &XconfLogger{
		SDKName: "xconf",
	}
}

func SetLevel(logLevel logrus.Level) {
	localLogger.SetLevel(logLevel)
}

type XconfLogger struct {
	SDKName string
}

func (logger *XconfLogger) Debugf(format string, params ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Debugf(format, params...)
}

func (logger *XconfLogger) Infof(format string, params ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Infof(format, params...)
}

func (logger *XconfLogger) Warnf(format string, params ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Warnf(format, params...)
}

func (logger *XconfLogger) Errorf(format string, params ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Errorf(format, params...)
}

func (logger *XconfLogger) Debug(v ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Debug(v...)
}

func (logger *XconfLogger) Info(v ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Info(v...)
}

func (logger *XconfLogger) Warn(v ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Warn(v...)
}

func (logger *XconfLogger) Error(v ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: logger.SDKName,
	}).Error(v...)
}

func Debugf(format string, params ...interface{}) {
	defaultLogger.Debugf(format, params...)
}

func Infof(format string, params ...interface{}) {
	defaultLogger.Infof(format, params...)
}

func Warnf(format string, params ...interface{}) {
	defaultLogger.Warnf(format, params...)
}

func Errorf(format string, params ...interface{}) {
	defaultLogger.Errorf(format, params...)
}

func Debug(v ...interface{}) {
	defaultLogger.Debug(v...)
}

func Info(v ...interface{}) {
	defaultLogger.Info(v...)
}

func Warn(v ...interface{}) {
	defaultLogger.Warn(v...)
}

func Error(v ...interface{}) {
	defaultLogger.Error(v...)
}

func loggerWith(logLevel logrus.Level) *logrus.Logger {
	logger := logrus.New()
	//All logs will be printed
	logger.SetLevel(logLevel)
	formatter := &nested.Formatter{
		NoColors:        false,
		TimestampFormat: "2006-01-02 15:04:05.000",
		HideKeys:        false,
		FieldsOrder:     []string{sdkField, filePathField},
	}
	logger.SetFormatter(formatter)

	//File name and line number display hook
	logger.AddHook(newFileHook())

	return logger
}

type fileHook struct{}

func newFileHook() *fileHook {
	return &fileHook{}
}

func (f *fileHook) Levels() []logrus.Level {
	return logrus.AllLevels
}

func (f *fileHook) Fire(entry *logrus.Entry) error {
	var s string
	_, b, c, _ := runtime.Caller(8)
	i := strings.LastIndex(b, "/")
	if i != -1 {
		s = b[i+1:len(b)] + ":" + IntToString(c)
	}
	entry.Data[filePathField] = s
	return nil
}

func IntToString(i int) string {
	return strconv.FormatInt(int64(i), 10)
}

type viperLogger struct {
	*logrus.Logger
	sdkName string
}

func (v viperLogger) Trace(msg string, keyvals ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: v.sdkName,
	}).Tracef(msg, keyvals...)
}

func (v viperLogger) Debug(msg string, keyvals ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: v.sdkName,
	}).Debugf(msg, keyvals...)
}

func (v viperLogger) Info(msg string, keyvals ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: v.sdkName,
	}).Infof(msg, keyvals...)
}

func (v viperLogger) Warn(msg string, keyvals ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: v.sdkName,
	}).Warnf(msg, keyvals...)
}

func (v viperLogger) Error(msg string, keyvals ...interface{}) {
	localLogger.WithFields(logrus.Fields{
		sdkField: v.sdkName,
	}).Errorf(msg, keyvals...)
}

func ViperLogger(logLevel logrus.Level) viper.Logger {
	localLogger = loggerWith(logLevel)
	return viperLogger{
		sdkName: "viper",
		Logger:  localLogger,
	}
}
