package v2

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"errors"
	"fmt"
	"path"
	"reflect"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	viper "xgit.xiaoniangao.cn/xngo/lib/github.com-spf13-viper"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/config"
	_ "xgit.xiaoniangao.cn/xngo/lib/xconf/v2/apolloviper/remote"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/common"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/logger"
)

type ConfigLogLevel int

const (
	LevelTrace ConfigLogLevel = iota
	LevelDebug
	LevelInfo
	LevelWarn
	LevelError
	LevelCritical
	LevelFatal
)

var SupportedExtensions = []string{"json", "toml", "yaml", "yml", "properties", "props", "prop", "hcl"}

type ViperConfigMap map[string]*XConfig

var configMap ViperConfigMap
var logLevel ConfigLogLevel

func logrusLogLevelWith(cfgLogLevel ConfigLogLevel) logrus.Level {
	level := logrus.InfoLevel
	switch cfgLogLevel {
	case LevelTrace:
		level = logrus.TraceLevel
	case LevelDebug:
		level = logrus.DebugLevel
	case LevelInfo:
		level = logrus.InfoLevel
	case LevelWarn:
		level = logrus.WarnLevel
	case LevelError:
		level = logrus.ErrorLevel
	case LevelCritical:
		level = logrus.PanicLevel
	case LevelFatal:
		level = logrus.FatalLevel
	}
	return level
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func SetLogLevel(levelParam ConfigLogLevel) {
	logLevel = levelParam

	level := logrusLogLevelWith(logLevel)
	logger.SetLevel(level)
}

func init() {
	logLevel = LevelInfo
	viperLogLevel := logrusLogLevelWith(logLevel)
	viper.SetLogger(logger.ViperLogger(viperLogLevel))
	SetLogLevel(logLevel)
}

func New() *XConfig {
	xViper := viper.New()
	return &XConfig{xViper, nil}
}

type XConfig struct {
	viper  *viper.Viper
	xParam *XParam
}

func (conf *XConfig) ReadConfig(content string) error {
	var contentBytes = []byte(content)
	return conf.viper.ReadConfig(bytes.NewBuffer(contentBytes))
}

// Unmarshal 非线程安全！！！
func (conf *XConfig) Unmarshal(rawVal interface{}) error {
	return conf.viper.Unmarshal(rawVal)
}

func (conf *XConfig) SetConfigName(in string) {
	conf.viper.SetConfigName(in)
}

func (conf *XConfig) SetConfigType(in string) {
	conf.viper.SetConfigType(in)
}
func (conf *XConfig) AddConfigPath(in string) {
	conf.viper.AddConfigPath(in)
}

func (conf *XConfig) ReadInConfig() error {
	return conf.viper.ReadInConfig()
}

func (conf *XConfig) Get(key string) interface{} {
	return conf.viper.Get(key)
}

func (conf *XConfig) GetString(key string) string {
	return conf.viper.GetString(key)
}

func (conf *XConfig) GetBool(key string) bool {
	return conf.viper.GetBool(key)
}

func (conf *XConfig) GetInt(key string) int {
	return conf.viper.GetInt(key)
}

func (conf *XConfig) GetInt32(key string) int32 {
	return conf.viper.GetInt32(key)
}

func (conf *XConfig) GetInt64(key string) int64 {
	return conf.viper.GetInt64(key)
}

func (conf *XConfig) GetFloat64(key string) float64 {
	return conf.viper.GetFloat64(key)
}

func (conf *XConfig) GetTime(key string) time.Time {
	return conf.viper.GetTime(key)
}

func (conf *XConfig) GetStringMap(key string) map[string]interface{} {
	return conf.viper.GetStringMap(key)
}

func (conf *XConfig) GetStringMapString(key string) map[string]string {
	return conf.viper.GetStringMapString(key)
}

// AllSettings 非线程安全！！！
func (conf *XConfig) AllSettings() map[string]interface{} {
	return conf.viper.AllSettings()
}

func (conf *XConfig) UnmarshalKey(key string, rawVal interface{}) error {
	return conf.viper.UnmarshalKey(key, rawVal)
}

// LoadConfigWithPassphrase ...
// confPath 配置文件路径  如：/data/server/pro/conf.yaml
// rawVal 配置文件映射的对象
// passphrase 可以传空串
func LoadConfigWithPassphrase(confPath string, rawVal interface{}, passphrase string) error {
	confPath = strings.Replace(confPath, "\\", "/", -1)
	fileDir := path.Dir(confPath)
	fileFullName := path.Base(confPath)
	fileExtension := path.Ext(fileFullName)
	if fileExtension == "" {
		fileExtension = ".yaml"
	}
	fileType := fileExtension[1:]
	if !stringInSlice(fileType, SupportedExtensions) {
		return viper.UnsupportedConfigError(fileType)
	}
	nameLen := len(fileFullName) - len(fileExtension)
	configName := fileFullName[:nameLen]
	conf := New()
	conf.SetConfigName(configName) // 配置文件的名字
	conf.SetConfigType(fileType)   // 配置文件的类型
	conf.AddConfigPath(fileDir)    // 配置文件的路径

	if err := conf.ReadInConfig(); err != nil {
		return err
	}

	if err := conf.Unmarshal(rawVal); err != nil {
		panic(fmt.Errorf("unable to decode into struct：  %s \n", err))
	}

	if passphrase != "" {
		if err := Decode(&rawVal, passphrase); err != nil {
			panic(fmt.Errorf("unalbe decrypt conf: %v", err))
		}
	}

	return nil
}

// LoadConfig ...
// confPath 配置文件路径  如：/data/server/pro/conf.yaml
// rawVal 配置文件映射的对象
func LoadConfig(confPath string, rawVal interface{}) error {
	return LoadConfigWithPassphrase(confPath, rawVal, "")
}

// InitWithConfigFileAndPassphrase ...
// xConfPath 配置文件路径  如：./xconf.yaml
// rawVal 配置文件映射的对象
func InitWithConfigFileAndPassphrase(xConfPath string, passphrase string) error {
	xParam := XParam{}
	err := LoadConfigWithPassphrase(xConfPath, &xParam, passphrase)
	if err != nil {
		return err
	}
	err = InitWithParam(xParam)

	return err
}

// InitWithConfigFile ...
// xConfPath 配置文件路径  如：./xconf.yaml
// rawVal 配置文件映射的对象
func InitWithConfigFile(xConfPath string) error {
	return InitWithConfigFileAndPassphrase(xConfPath, "")
}

// InitWithParam ...
func InitWithParam(param XParam) error {
	logPrefix := "[InitWithParam]=> "
	logger.Info(logPrefix, "start init...")
	if param.Ip == "" {
		errMsg := "fail to init, ip is empty"
		logger.Error(logPrefix, errMsg)
		return errors.New(errMsg)
	}
	if param.AppId == "" {
		errMsg := "fail to init, AppId is empty"
		logger.Error(logPrefix, errMsg)
		return errors.New(errMsg)
	}
	param.resetParam()

	appID := param.AppId
	cluster := param.Cluster
	ip := param.Ip
	namespace := param.Namespace
	backupDir := param.BackupDir

	configMap = make(ViperConfigMap)
	apolloviper.InitViperConfigMap(appID, ip, cluster, namespace, backupDir)
	viperMap := apolloviper.GetConfigMap()

	for key, viperValue := range viperMap {
		conf := &XConfig{viperValue, &param}
		configMap[key] = conf
	}

	logger.Info(logPrefix, "end init")
	return nil
}

// GetConfig ...
func GetConfig(namespace string, rawVal interface{}) (xConfig *XConfig, err error) {
	if len(configMap) < 1 {
		errorMessage := "No initialization. Need call InitWithParam."
		return nil, errors.New(errorMessage)
	}
	xConfig = configMap[namespace]
	if xConfig == nil {
		errorMessage := fmt.Sprintf("namespace:%v load failed. config is nil", namespace)
		return nil, errors.New(errorMessage)
	}
	if rawVal != nil {
		err = xConfig.Unmarshal(rawVal)
	}
	return
}

// AddChangeListener ...
func AddChangeListener(listener common.ChangeListener) {
	config.AddChangeListener(listener)
}

// Decode, decode encrypt pass
func Decode(obj interface{}, key string) error {
	var v reflect.Value
	if ov, ok := obj.(reflect.Value); ok {
		v = ov
	} else {
		v = reflect.ValueOf(obj)
	}
	switch v.Kind() {
	case reflect.Ptr:
		return Decode(v.Elem(), key)
	case reflect.String:
		str := v.String()
		if v.CanSet() && strings.HasPrefix(str, "ENC~") {
			text, err := Decrypt(str[4:], key)
			if err != nil {
				return err
			}
			v.SetString(string(text))
		}
	case reflect.Struct:
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			if err := Decode(field, key); err != nil {
				return err
			}
		}
	case reflect.Slice | reflect.Array:
		l := v.Len()
		for i := 0; i < l; i++ {
			if err := Decode(v.Index(i), key); err != nil {
				return err
			}
		}
	case reflect.Map:
		for _, k := range v.MapKeys() {
			vm := v.MapIndex(k)
			switch vm.Kind() {
			case reflect.Ptr:
				return Decode(v.Elem(), key)
			case reflect.String:
				str := vm.String()
				if v.CanSet() && strings.HasPrefix(str, "ENC~") {
					text, err := Decrypt(str[4:], key)
					if err != nil {
						return err
					}
					val := reflect.ValueOf(string(text))
					v.SetMapIndex(k, val)
				}
			case reflect.Struct:
				newV := reflect.New(vm.Type())
				for i := 0; i < vm.NumField(); i++ {
					field := vm.Field(i)
					if ok, newStr, err := hasDecodeString(field, key); ok == true && err == nil {
						newV.Elem().Field(i).SetString(newStr)
					} else {
						if err != nil {
							return err
						} else {
							newV.Elem().Field(i).Set(field)
						}
					}
				}
				v.SetMapIndex(k, newV.Elem())
			default:
				return Decode(vm, key)
			}
		}
	case reflect.Interface:
		return Decode(v.Interface(), key)
	}
	return nil
}

// DecodeMapElem decode encrypt pass
func DecodeMapElem(obj interface{}, key string) (rewrite bool, s reflect.Value, err error) {
	var v reflect.Value
	if ov, ok := obj.(reflect.Value); ok {
		v = ov
	} else {
		v = reflect.ValueOf(obj)
	}
	switch v.Kind() {
	case reflect.Ptr:
		err := Decode(v.Elem(), key)
		return false, s, err
	case reflect.Struct:
		newV := reflect.New(v.Type())
		for i := 0; i < v.NumField(); i++ {
			field := v.Field(i)
			if ok, newStr, err := hasDecodeString(field, key); ok == true && err == nil {
				newV.Elem().Field(i).SetString(newStr)
			} else {
				if err != nil {
					return rewrite, s, err
				} else {
					newV.Elem().Field(i).Set(field)
				}
			}
		}
		return true, newV, err
	}
	return
}

// hasDecodeString 是否包含需要解密的字符串
// bool 是否存在需要解密的字符串
// string 新的字符串
// error 解析的错误
func hasDecodeString(v reflect.Value, key string) (bool, string, error) {
	switch v.Kind() {
	case reflect.String:
		str := v.String()
		if strings.HasPrefix(str, "ENC~") {
			text, err := Decrypt(str[4:], key)
			if err != nil {
				return true, "", err
			}
			return true, string(text), nil
		}
	default:
		return false, "", Decode(v, key)
	}
	return false, "", nil
}

func Decrypt(text string, passphrase string) ([]byte, error) {
	data, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return nil, errors.New("Invalid text to decrypt")
	}
	key := []byte(passphrase)
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}
	return plaintext, nil
}
