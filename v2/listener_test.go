package v2

import (
	"fmt"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/common"
)

var propMap sync.Map

func init() {
	propMap.Store(x2Json, Application{})
	propMap.Store(applicationProperties, Application{})
	propMap.Store(xconfServerYaml, Conf{})
}

func TestRemoteChangeListener(t *testing.T) {
	namespaceList := []string{applicationProperties, xconfServerYaml, x2Json}
	param := defaultXParam
	param.Namespace = getSplitCommaNamespace(namespaceList...)

	err := InitWithParam(param)
	assert.NoError(t, err)
	AddChangeListener(tListener)

	du := time.Millisecond * 800
	var tch = time.NewTimer(du)
	count := 1
	countLimit := 10
	for {
		select {
		case <-tch.C:
			go func() {
				assertWithNamespace(t, namespaceList...)
			}()
			if count > countLimit {
				return
			}
			count += 1

			tch.Reset(du)
		}
	}
}

type testListener struct {
}

func (t testListener) OnChange(event *common.ChangeEvent) {
	fmt.Println(event.Namespace)
	namespace := event.Namespace
	var value interface{}
	switch namespace {
	case applicationProperties:
		value = &Application{}
	case x2Json:
		value = &Application{}
	case xconfServerYaml:
		value = &Conf{}
	}
	_, err := GetConfig(namespace, value)
	if err == nil {
		propMap.Store(namespace, value)
	}

	if v, ok := propMap.Load(event.Namespace); ok {
		fmt.Printf("\nchange for app: %v\n\r", v)
	}

}

var tListener testListener
