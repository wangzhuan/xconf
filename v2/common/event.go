package common

//ChangeListener ...
type ChangeListener interface {
	//OnChange 增加变更监控
	OnChange(event *ChangeEvent)
}

//ChangeEvent ...
type ChangeEvent struct {
	Namespace string
}
