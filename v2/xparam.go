package v2

import (
	"fmt"
	"net/url"
	"path"
	"strings"

	"xgit.xiaoniangao.cn/xngo/lib/xconf/v2/logger"
)

// XParam ...
type XParam struct {
	AppId     string `mapstructure:"appId"`     // 对应于apollo 的 appId
	Cluster   string `mapstructure:"cluster"`   // 对应apollo集群，默认为default
	Namespace string `mapstructure:"namespace"` // 对应于apollo 的 namespace， 默认为application，按"，"分隔，如 app.yaml,x2.properties
	Ip        string `mapstructure:"ip"`        // 为apollo config server 的 SLB地址加端口
	BackupDir string `mapstructure:"backupDir"`
}

func (param *XParam) resetParam() {
	if param.Namespace == "" {
		param.Namespace = param.AppId
	}
	namespaces := strings.Split(param.Namespace, ",")

	if len(namespaces) == 1 {
		namespace := namespaces[0]
		suffix := strings.ToLower(path.Ext(namespace))
		if suffix == "" {
			suffix = "yaml"
			param.Namespace = fmt.Sprintf("%s.%s", namespace, suffix)
		}
	}
	if !strings.HasPrefix(param.Ip, "http") {
		// param.Ip 目前都是用的这种形式：192.168.99.252:8080
		param.Ip = fmt.Sprintf("http://%s", param.Ip)
	}
	_, err := url.Parse(param.Ip)
	if err != nil {
		errMsp := "invalid param.Ip,  url.Parse err:"
		logger.Error(errMsp, err)
		panic(errMsp)
		return
	}

	if param.Cluster == "" {
		param.Cluster = "default"
	}
}
