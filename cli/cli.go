package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"flag"
	"fmt"
	"io"
	randMath "math/rand"
	"os"
	"time"
)

func Encrypt(data []byte, passphrase string) (string, error) {
	block, err := aes.NewCipher([]byte(passphrase))
	if err != nil {
		return "", err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}
	seal := gcm.Seal(nonce, nonce, data, nil)
	return base64.StdEncoding.EncodeToString(seal), nil
}

func EncryptString(s string, key string) (string, error) {
	s, err := Encrypt([]byte(s), key)
	if err != nil {
		return "", err
	}
	return "ENC~" + s, nil
}

const (
	LetterBytes   = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	RandStringLen = 32
)

func getPassphrase(n int) string {
	randMath.Seed(time.Now().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = LetterBytes[randMath.Intn(len(LetterBytes))]
	}
	return string(b)
}

func main() {
	var (
		secret     string
		passphrase string
	)
	flag.StringVar(&secret, "secret", "", "set secret")
	flag.StringVar(&passphrase, "passphrase", "", "set passphrase")
	flag.Parse()

	if secret == "" {
		fmt.Printf("please input secret string\n")
		os.Exit(0)
	}

	if passphrase == "" {
		passphrase = getPassphrase(RandStringLen)
	}

	encryptStr, err := EncryptString(secret, passphrase)
	if err != nil {
		fmt.Printf("发生错误：%v\n", err)
		os.Exit(2)
	}
	fmt.Printf("key:%s\nsecret：%s\n", passphrase, encryptStr)
}
