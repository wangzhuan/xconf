# xconf 应用配置加载

## Features

1. 支持传入struct获取配置内容
2. 支持通过配置文件key获取配置内容
3. 通过URL加载远程配置

### 支持yaml配置文件

配置文件定义如：
```yaml
db_servers:
  - &group_db "mongodb://dbhost0-1.xiaoniangao.cn:27017"
  - &content_filter_db "mongodb://120.25.66.46:27077"

redis_servers:
  - &filter_redis {address: "redis-0.xiaoniangao.cn:6379", auth: "112"}
  - &count_redis {address: "127.0.0.1:6379", auth: "115"}

app_name: "XConfig SDK"

mongo:
  content_filter:
    dsn: *content_filter_db
  xng_group:
    dsn: *group_db
  xng_group_0:
    dsn: *group_db
  xng_group_1:
    dsn: *group_db

redis:
  content_filter:
    <<: *filter_redis
    db: 2
  share_count:
    <<: *count_redis
    db: 1
  praise_count:
    <<: *count_redis
    db: 0
  follow_count:
    <<: *count_redis
    db: 0
```

### 通过传入struct获取配置内容

struct定义如下：
```go
type RedisConf struct {
	Address string `mapstructure:"address"`
	Auth string `mapstructure:"auth"`
	Db   int    `mapstructure:"db"`
}
type MongoConf struct {
	Dsn    string `mapstructure:"dsn"`
	DbName string
}
type Conf struct {
	Redis map[string]RedisConf `mapstructure:"redis"`
	Mongo map[string]MongoConf `mapstructure:"mongo"`
}

```
加载本地配置：
```go
// 追踪配置文件的每一步加载
SetLogLevel(LevelTrace)

filePath := "./conf.yaml"
configObj := Conf{}
err := LoadConfig(filePath, &configObj)
if err != nil {
    t.Fatal(err)
}
```

### 通过创建XConfig获取配置内容

```go
xConfig := New()
xConfig.SetConfigName("conf")
xConfig.SetConfigType("yaml")
xConfig.AddConfigPath(".")
err := xConfig.ReadInConfig() // Find and read the config file
if err != nil { // Handle errors reading the config file
    t.Fatal(err)
}
appName := xConfig.GetString("app_name")
redisDb := xConfig.GetInt("redis.content_filter.db")
mongoMap := xConfig.GetStringMap("mongo")
```


## 加载远程配置(apollo动态配置中心)

###直接传参的形式调用
```go
// 追踪配置文件的每一步加载
SetLogLevel(LevelTrace)

param := XParam{
    Ip:         "192.168.99.252:8080", 
    AppId:      "SampleApp",
    Namespace:  "application",  // 可选
    ConfigType: "properties",           //可选
    BackupFileName: "/data/server/xxx/application.json" //可选
}
// 以上参数，Ip, AppId不能为空，其他参数可选
// 如果只设置了Ip和AppId参数，则apollo配置项里必须存在名字为AppId的namspace，并且类型为yaml
// ConfigType默认为yaml, 支持json, properties, yaml
// 如果只有Ip和AppId，则Namespace=AppId
configObj := Conf{}
x1, err := NewWithParam(param)
assert.NoError(t, err)

err = x1.Unmarshal(&configObj)
assert.NoError(t, err)
checkConfigObject(configObj, t)

```

###采用加载本地配置文件（内容对应于XParam）的方式
配置文件如下：

```yaml
ip: "192.168.99.252:8080"
appId: "bxd"
```

调用NewWithConfigFile加载

```go
// 追踪配置文件的每一步加载
SetLogLevel(LevelTrace)

// 采用加载本地配置文件（内容对应于XParam）的方式
filePath := "./xconf.yaml"
configObj := Conf{}
xConfig, err := NewWithConfigFile(filePath, &configObj)
assert.NoError(t, err)
checkConfigObject(configObj, t)

appName := xConfig.GetString("app_name")
assert.Equal(t, "XConfig SDK", appName)
```