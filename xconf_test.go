package xconf

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type RedisConf struct {
	Address string `mapstructure:"address"`
	Auth    string `mapstructure:"auth"`
	Db      int    `mapstructure:"db"`
}
type MongoConf struct {
	Dsn    string `mapstructure:"dsn"`
	DbName string
}
type Conf struct {
	Redis   map[string]RedisConf `mapstructure:"redis"`
	Mongo   map[string]MongoConf `mapstructure:"mongo"`
	AppName string               `mapstructure:"app_name"`
}

func checkConfigObject(configObj Conf, t *testing.T) {
	assert.NotEmpty(t, configObj.Redis["content_filter"])
	assert.NotEmpty(t, configObj.Mongo["content_filter"])

	assert.Equal(t, "mongodb://120.25.66.46:27077", configObj.Mongo["content_filter"].Dsn)
	assert.Equal(t, "redis-0.xiaoniangao.cn:6379", configObj.Redis["content_filter"].Address)
	assert.Equal(t, "112", configObj.Redis["content_filter"].Auth)
	assert.Equal(t, 2, configObj.Redis["content_filter"].Db)
}

func TestLoadConfig(t *testing.T) {
	// 追踪配置文件的每一步加载
	SetLogLevel(LevelTrace)

	filePath := "./conf.yaml"
	configObj := Conf{}
	err := LoadConfig(filePath, &configObj)
	if err != nil {
		t.Fatal(err)
	}
	checkConfigObject(configObj, t)
}

func TestXConfig(t *testing.T) {
	xConfig := New()
	xConfig.SetConfigName("conf")
	xConfig.SetConfigType("yaml")
	xConfig.AddConfigPath(".")
	err := xConfig.ReadInConfig() // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		t.Fatal(err)
	}
	configObj := Conf{}
	err = xConfig.Unmarshal(&configObj)
	if err != nil {
		t.Fatal(err)
	}

	checkConfigObject(configObj, t)

	appName := xConfig.GetString("app_name")
	redisDb := xConfig.GetInt("redis.content_filter.db")
	mongoMap := xConfig.GetStringMap("mongo")

	assert.Equal(t, true, len(mongoMap) > 0)
	assert.Equal(t, "XConfig SDK", appName)
	assert.Equal(t, 2, redisDb)
}

func TestRemoteXConfig(t *testing.T) {
	param := XParam{
		Ip:         "192.168.99.252:8080",
		AppId:      "SampleApp",
		Namespace:  "application",
		Cluster:    "default",
		ConfigType: "properties",
	}
	xConfig, err := NewWithParam(param)
	assert.NoError(t, err)

	appName := xConfig.GetString("app_name")
	assert.Equal(t, "XConfig SDK", appName)
}

func TestRemoteYamlXConfig(t *testing.T) {
	// 目前用agollo有bug ，先用namespace 为 application
	param := XParam{
		Ip:         "192.168.99.252:8080",
		AppId:      "SampleApp",
		Namespace:  "dev_test",
		ConfigType: "yaml",
	}
	configObj := Conf{}
	x1, err := NewWithParam(param)
	assert.NoError(t, err)

	err = x1.Unmarshal(&configObj)
	assert.NoError(t, err)
	checkConfigObject(configObj, t)
}

func TestXConfigLoadXconfFile(t *testing.T) {
	filePath := "./xconf.yaml"
	xParam := XParam{}
	err := LoadConfig(filePath, &xParam)
	assert.NoError(t, err)

	configObj := Conf{}
	x1, err := NewWithParam(xParam)
	assert.NoError(t, err)
	err = x1.Unmarshal(&configObj)
	assert.NoError(t, err)
	checkConfigObject(configObj, t)
}

func TestNewWithConfigFile(t *testing.T) {
	SetLogLevel(LevelTrace)
	filePath := "./xconf.yaml"
	configObj := Conf{}
	xConfig, err := NewWithConfigFile(filePath, &configObj, "")
	assert.NoError(t, err)
	checkConfigObject(configObj, t)

	appName := xConfig.GetString("app_name")
	assert.Equal(t, "XConfig SDK", appName)
}
